## Presentación
Esto no es más que un blog personal escrito con ganas de compartir algunas
inquietudes, experiencias y algo de conocimiento.

Este blog está elaborado utilizando [Hugo](https://gohugo.io/) y
[GitLab Pages](https://gitlab.com/pages/hugo).

## Clonado del proyecto

`git clone --recursive` [url del proyecto]

Esto hará que, además de descargar el repositorio, descargue los módulos que
utilizo (ej. theme / tema beautifulhugo)

## Ejecutar en local

Si clonas o descargas este proyecto a tu ordenador personal utiliza `hugo server`,
para ejecutarlo. El blog estará accesible bajo `localhost:1313/`.

El tema utilizado es una adaptación de http://themes.gohugo.io/beautifulhugo/.

---
title: Recetas y apuntes
subtitle: Blog personal de pmoya_in_the_web
date: 2020-05-31
comments: true
---

Esto no es más que un blog personal escrito con ganas de compartir algunas
inquietudes, experiencias y algo de conocimiento.

No te engañaré, no sólo es la difusión lo que me trae por aquí. También pretendo
utilizarlo para motivarme a aprender cosas nuevas, organiarlas en mi cabeza y,
por supuesto, como referencia para mi mismo en el futuro.

Si quieres contactar conmigo puedes hacerlo a través de;
- Correo electronico; **pmoya-in-the-web** [arroba] **tutanota.com**
- Mastodon; [@pmoya_in_the_web](https://mastodon.madrid/@pmoya_in_the_web)

Este blog está elaborado utilizando [Hugo](https://gohugo.io/) y
[GitLab Pages](https://gitlab.com/pages/hugo). Seguro que habría sido más sencillo
utilizar un [CMS](https://en.wikipedia.org/wiki/Content_management_system#Best_known_CMSs)
tipo [Wordpress](https://en.wikipedia.org/wiki/WordPress),
[Drupal](https://en.wikipedia.org/wiki/Drupal) o cualquier otro con una buena
interfaz gráfica. Me remito a mi primera motivación para iniciar este blog ...
"aprender cosas nuevas".

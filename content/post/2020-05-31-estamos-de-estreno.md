---
title: "Estamos de estreno"
date: 2020-05-31
tags: []
---

¡Por fin llegó el día!

Tras un poco de investigación, y mucha
[procastinación](https://dle.rae.es/procrastinaci%C3%B3n), me he decidido a
lanzar este blog personal. Veremos en que se que se convierte y si consigo tener
la suficiente disciplina para actualizarlo con cierta frecuencia ...

También tengo pendiente adaptar el aspecto visual y añadir algunas secciones,
agregar la posibilidad de dejar comentarios en los artículos (estoy decidiéndome
entre usar [Mastondon](https://es.wikipedia.org/wiki/Mastodon_(red_social)) o
bien [Webmentions](https://indieweb.org/Webmention)) y, por supuesto, postear más
y más contenido.

*Invitados quedáis a visitarlo*

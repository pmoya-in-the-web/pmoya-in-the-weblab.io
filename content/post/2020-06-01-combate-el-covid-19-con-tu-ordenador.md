---
title: "Combate el COVID-19 con tu ordenador"
date: 2020-06-01
tags: ["boinc","ibercivis","covid-19"]
---

# ¿Te gustaría ayudar a combatir el COVID-19 desde casa?
Sólo necesitas dos cosas, la primera es un poco de curiosidad y la
segunda es tu ordenador (cuantos más, mejor).

En la actualidad, y gracias a los ordenadores de miles y miles de
empresas, Administraciones Públicas y particulares distribuidos por todo
el mundo, varias instituciones científicas están realizando simulaciones
informáticas y experimentos con el objetivo de combatir esta enfermedad.
Tú, y todos, podemos ayudarles poniendo a su disposición nuestros
ordenadores. Cuantos más seamos, más pronto entenderemos cómo funciona
el virus, cómo se comporta ante distintos fármacos actuales o futuros e
incluso descubrir una vacuna.

# ¿Qué tengo que hacer exactamente?
Vamos al grano, desde aquí te planteamos dos opciones; Boinc (a través
del proyecto Rosetta\@home) y Folding\@home. Sólo tendrás que instalar y
configurar una de las dos opciones que aquí te proponemos..

## Boinc
[*Boinc*](https://boinc.berkeley.edu/) es una plataforma que da cabida a
múltiples proyectos científicos en los que puedes colaborar (este es su
principal punto fuerte). Ahora mismo te recomendamos que te suscribas al
proyecto COVID-PHYM a través de
[*ibercivis*](https://boinc.ibercivis.es/ibercivis) y el proyecto
[*Rosetta\@home*](https://boinc.bakerlab.org/rosetta/), que en este
momento  está realizando investigaciones para combatir el COVID-19.

Para ponerlo a funcionar sólo tienes que
[*descargar*](https://boinc.berkeley.edu/download.php) e instalar el
software y añadir el proyecto ibercivis ( *Menú - Herramientas / Añadir
proyecto / URL del proyecto:*
[*https://boinc.ibercivis.es/ibercivis*](https://boinc.ibercivis.es/ibercivis)
)  y/o Rosetta (*Menú - Herramientas / Añadir proyecto /
Rosetta\@home*). Si no tienes cuenta puedes crearla al añadir el
proyecto y sin salir de la aplicación.

Puedes leer más información sobre los proyectos mencionados aquí:

* Proyecto COVID-PHYM a través de **Ibercivis** en: [*web del proyecto*](https://boinc.ibercivis.es/ibercivis) o en
  este [*vídeo*](https://www.youtube.com/watch?v=272XjugZeDI).

  {{< youtube 272XjugZeDI >}}

  La [*fundación Ibercivis*](https://ibercivis.es/) está contribuyendo al
  proyecto [*COVID-PHYM del CSIC*](https://ibercivis.es/project/proyecto-covid-phym/)
  (investigación "*made in Spain"*).

* **Rosetta\@home** en: [*wikipedia*](https://es.wikipedia.org/wiki/Folding@home)
  [*eng*](https://en.wikipedia.org/wiki/Folding@home),
  [*web del proyecto*](https://boinc.bakerlab.org/rosetta/), o en su cuenta de
  [*twitter*](https://twitter.com/rosettaathome).


## Folding\@home

Esta segunda opción es más interesante si tu ordenador cuenta con una
buena GPU (Nvidia / ATI) ya que está optimizado para ejecutarse en en
ese tipo de hardware, aunque recientemente también han implementado la
capacidad de ejecutarse también en CPU.

Descargar [*Folding\@home*](https://foldingathome.org/) desde
[*aquí*](https://foldingathome.org/alternative-downloads/) y
configúralo, es muy sencillo.

Puedes leer más información acerca del proyecto Folding\@home en;
[*wikipedia*](https://es.wikipedia.org/wiki/Rosetta@home)
[*eng*](https://en.wikipedia.org/wiki/Rosetta@home), [*web del
proyecto*](https://foldingathome.org/), o en su cuenta de
[*twitter*](https://twitter.com/foldingathome). También te dejamos
algunos artículos en medios
[*1*](https://hipertextual.com/2020/04/coronavirus-convirtio-foldinghome-supercomputadoras-mas-potente-mundo-24-exaflops),
[*2*](https://www.xataka.com/otros/nvidia-nos-propone-como-poner-sus-graficas-al-servicio-investigacion-covid-20-iniciativa-folding-home),
[*3*](https://victorhckinthefreeworld.com/2020/04/05/pon-la-potencia-de-tu-linux-a-luchar-contra-el-covid-19/)
e incluso un [*vídeo*](https://www.youtube.com/watch?v=4d7UxMvg5sU).

{{< youtube 4d7UxMvg5sU >}}

# Pero, ¿cómo funciona esto exactamente?
Básicamente es una estrategia del tipo "[*divide y
vencerás*](https://es.wikipedia.org/wiki/Algoritmo_divide_y_vencerás)"
utilizando [*computación
distribuida*](https://es.wikipedia.org/wiki/Computación_distribuida).

El tipo de simulaciones informáticas que hacen los científicos son muy
complejas y requieren ordenadores con una capacidad de cómputo muy
grande. Incluso para estos, el trabajo a realizar les lleva mucho
tiempo. El trabajo a realizar se divide y trocea en pedazos más pequeños
(pero mucho más fáciles de resolver) que pueden ser resueltos de forma
independiente por múltiples ordenadores al mismo tiempo. Una vez
resueltos se juntan dando así solución al problema inicialmente
planteado. Tu ordenador puede solventar sólo una pequeña parte del total
de cálculos, pero unido a los de otras miles y miles de personas pueden
lograr cosas
[*asombrosas*](https://www.elespanol.com/omicrono/tecnologia/20200326/gente-ayuda-cura-coronavirus-superado-superordenadores/477703637_0.html).

Los programas que comentábamos anteriormente
([*Boinc*](https://boinc.berkeley.edu/) y
[*Folding\@home*](https://foldingathome.org/)) son las dos principales
opciones que existen ahora mismo. Cabe reseñar que Boinc da cabida a
multitud de proyectos de distinta índole científica.

# Algunas recomendaciones
Una vez instalado el programa revisa el consumo de CPU, memoria RAM y
almacenamiento asignado para que, aunque esté funcionando, puedas seguir
haciendo uso del ordenador sin que impacte en las labores del día a día.
También existe una opción para "suspender" / "pausar" el programa
mientras haces un uso intensivo de tu ordenador. Puedes indicar que, por
ejemplo, cuando el uso del procesador sea mayor al 60% este quede
pausado.

Si estás utilizando tu portátil presta especial atención a la
temperatura: un consumo elevado de CPU puede hacer que esta suba
demasiado. Te recomendamos que el valor asignado se encuentre entre el
50% y 60% para que el dispositivo no sufra. En dispositivos sobremesa
deberías poder subir el consumo hasta el 80%, o incluso al 100% sin
problema.

Ten en cuenta que una vez registrado puedes utilizar tus credenciales en
tantos dispositivos como quieras: en tu portátil, sobremesa o en el del
trabajo (*de momento sólo si tu usuario tiene permisos de
administrador*). Incluso lo puedes ejecutar en tu dispositivo [*tablet o
smartphone*](https://play.google.com/store/apps/details?id=edu.berkeley.boinc&hl=es).

Para los usuarios de Linux, recordad que probablemente tengáis que
activar el servicio para que se ejecute al iniciar el sistema. Aquí os
dejamos un artículo que habla sobre su [*instalación y
configuración*](https://victorhckinthefreeworld.com/2020/04/05/pon-la-potencia-de-tu-linux-a-luchar-contra-el-covid-19/).

# ¿Hay más iniciativas similares a las comentadas anteriormente?
Además de las ya comentadas, hay más iniciativas en la misma línea.
[*Aquí*](https://medium.com/adventures-in-volunteer-computing/your-computer-can-do-coronavirus-research-5f2fcfc502d4)
puedes consultar un resumen de estas y, además, te dejamos referencia
directa a las que más nos han gustado.

-   [*Foldit*](https://fold.it/)
    ([*1*](https://www.youtube.com/user/UWfoldit/featured)), videojuego
    / rompecabezas interactivo.
-   [*TACC\@home*](https://boinc.tacc.utexas.edu/)
    ([*1*](https://www.tacc.utexas.edu/-/for-the-love-of-science)) a
    través del software [*Boinc*](https://boinc.berkeley.edu/).
-   [*OpenPandemics*](https://www.ibm.org/OpenPandemics) (subproyecto de
    [*WorldCommunityGrid*](https://www.worldcommunitygrid.org/discover.action))
    a través del software [*Boinc*](https://boinc.berkeley.edu/).

# Y por último
Si te decidiste por utilizar Boinc, recuerda que
[*ibercivis*](https://boinc.ibercivis.es/ibercivis),
[*Rosetta\@home*](https://boinc.bakerlab.org/rosetta/),
[*WorldCommunityGrid*](https://www.worldcommunitygrid.org/discover.action) o
[*TACC\@home*](https://boinc.tacc.utexas.edu/) no son los únicos
proyectos en el que podrás contribuir. Además de todos los anteriores
tienes a tu disposición[
](https://boinc.berkeley.edu/projects.php)[*muchos
más*](https://boinc.berkeley.edu/projects.php) de diversas instituciones
(incluso otra, además de ibercivis, muy cercana como
[*GPUGrid*](https://www.gpugrid.net/) de la Universidad Pompeu Fabra en
Barcelona) y temáticas (salud, física, matemáticas, climatología ...).

**Anímate, tu contribución a la ciencia no debe ser algo puntual sino que
es un camino que nunca termina.**


# Bonus track
Boinc integra un sistema de puntos, cuanto más tiempo empleas más puntos obtienes.
Puedes ir en solitario o también en equipos. Es una forma estupenda de "picarse"
para subir en el ranking *;-)*

[*Aquí tenéis mis estadísticas personales*](https://www.boincstats.com/stats/-1/user/detail/98020623864/projectList)

{{< figure src="https://www.boincstats.com/signature/-1/user/98020623864/sig.png"
    alt="Estadísticas personales en Boinc" >}}
<br>

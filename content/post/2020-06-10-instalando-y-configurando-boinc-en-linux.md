---
title: "Instalando y configurando Boinc en linux"
date: 2020-12-07
# image: "third_party/third-party/BOINC_logo_July_2007.svg"
tags: ["boinc","ibercivis","covid-19", "linux", "opensuse", "ubuntu"]
draft: true
---

En mi [anterior post](../2020-06-01-combate-el-covid-19-con-tu-ordenador)
hablaba sobre cómo cualquiera de nosotros puede contribuir con su granito de arena
para encontrar una vacuna y / o tratamiento para combatir el COVID-19.

En este artículo vamos al grano, ¿cómo instalamos y configuramos Boinc?

Antes de que continues leyendo o lo abandones, ten en cuenta una cosa, *da igual
si tu ordenador es o no el más potente del mundo, si tiene 7 años o si te compraste
una tarjeta gráfica de 700 euros. La cuestión es contribuir.*, cuantos más seamos
mejor.

# Instalación


## Opensuse

lo trae por defecto ... si os gusta instalarlo a través de la línea de comandos sólo tendréis que abrir una ventana del terminal y ...

> sudo zypper --non-interactive in boinc-client boinc-manager
>
> sudo systemctl start boinc-client
>
> sudo usermod -G boinc -a $USER

https://wiki.archlinux.org/index.php/BOINC

si por el contrario preferís utilizar Yast, sólo hay que ...


## Ubuntu, Linux Mint o Elementary OS

## Zorin OS

## Fedora

## Solus

# Configuración de proyectos
https://boinc.berkeley.edu/wiki/Boinccmd_tool


echo "BOINC service (boinc-client) will be running on background and started automatically when your system starts"
echo "The user " $USER " has been configured in order to be able to execute boinc-manager, if you want other users to to it type; usermod -G boinc -a userName"
echo "First time you launch Boinc Manager go to \"Archive / Select computer\" and connect to 127.0.0.1, password is under /var/lib/boinc/gui_rpc_auth.cfg"
echo "Remember to add projects and configure computing preferences"


# ¿Qué son los equipos / teams?
Grupo de voluntarios de la computación distribuida a través de la plataforma BOINC.
Información de todos los proyectos en general y de Ibercivis en particular.
Puedes unirte en https://t.me/CANALBoinc
